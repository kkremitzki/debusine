.. _setting-up-debusine-client:

==========================
Setting up debusine-client
==========================

Introduction
------------

The ``debusine-client`` package provides the command ``debusine``. Install the
``debusine-client`` package. The :ref:`Initial setup using debusine server<initial-setup-debusine-server>`
has the instructions how to add the debusine repository.

``debusine-client`` command is used to submit work requests to the
Debusine server and to look into the status of the requests.

Initial setup
-------------

The ``debusine-client`` needs the Debusine server URL and an enabled token for the
server. The Debusine Server administrator can provide you with a token. The section
:ref:`Creating a token for a Debusine Client <creating-token-for-debusine-client>`
explains how to create a token for a client.


  #. Create the directory for the ``config.ini`` file:

     .. code-block:: console

       $ mkdir --parents "$HOME/.config/debusine/client"

  #. Copy the example ``config.ini`` file to this directory:

     .. code-block:: console

       $ cp /usr/share/doc/debusine-client/examples/config.ini "$HOME/.config/debusine/client/"

  #. Edit the ``config.ini`` file:

     .. code-block:: console

       $ editor "$HOME/.config/debusine/client/config.ini"
  
  #. Rename ``[server:localhost]`` to ``[server:server_name]`` and set
     ``default-server = server_name``. The Debusine client supports multiple
     servers, one of which is a default server. It is possible to specify to which server
     the Debusine client connects using the argument ``--server NAME``.


  #. Set the ``url`` and ``token``. The Debusine Server admin should create a token
     on the server for this client.

.. _submitting-a-work-request:
Submitting a work request
-------------------------

To submit a work request:

  #. Create a file with the work request, for example:

     .. code-block:: console

      $ cat > work-request-hello.debusine << END
      build_components:
      - any
      - all
      distribution: stable
      host_architecture: amd64
      input:
        source_package_url: https://deb.debian.org/debian/pool/main/h/hello/hello_2.10-2.dsc
      END

     It is possible to add sbuild options. For example, to add
     ``post-build-commands`` include the ``sbuild_options`` section:

     .. code-block:: console

      $ cat > work-request-hello.debusine << END
      build_components:
      - any
      - all
      distribution: stable
      host_architecture: amd64
      input:
        source_package_url: https://deb.debian.org/debian/pool/main/h/hello/hello_2.10-2.dsc
      sbuild_options:
      - --post-build-commands=/bin/cp %SBUILD_CHANGES /tmp/
      END

  #. Submit the work request of type sbuild:

     .. code-block:: console

       $ debusine create-work-request sbuild < work-request-hello.debusine

     The command will show an output such as:

     .. code-block:: yaml

       result: success
       message: Work request registered on http://localhost/api with id 5.
       work_request_id: 5

  #. Check the status of the work request:

     .. code-block:: console

       $ debusine show-work-request 5

  #. If the status shows ``status: pending`` without a ``worker`` it might indicate
     that there is no ``debusine-worker`` available yet. At some point, the
     work request will have a ``worker`` assigned and then the status should be
     ``running``, then ``completed`` or ``aborted``.

     Once the status is ``completed``, the field ``result`` will be ``success``
     or ``failure``.


.. _creating-an-artifact:
Creating an artifact
--------------------

To create an artifact for the category ``Test`` uploading ``FILE1`` and ``FILE2``:

.. code-block:: console

 $ debusine create-artifact Test --workspace System --upload FILE1 FILE2
 result: success
 message: New artifact created in http://localhost:8011/api in workspace System with id 61.
 artifact_id: 61
 files_to_upload: 2

The artifact is creating and the file are uploaded automatically.

Check the documentation for the create-artifact regarding uploading multiple
files, files relative to a directory, etc. using:

.. code-block:: console

 $ debusine create-artifact --help


The artifact can be downloaded via:

.. code-block:: console

 $ debusine download-artifact ARTIFACT_ID

.. _creating-work-request-using-an-artifact:
Creating an sbuild work-request using an artifact
-------------------------------------------------

In the example above sbuild downloaded the .dsc file.

In this section debusine will download the artifact and sbuild will use
the .dsc (and other required files) from the artifact.

#. Prepare a directory with the relevant files

   .. code-block:: console

    $ mkdir temp_directory/
    $ cd temp_directory/
    $ wget http://deb.debian.org/debian/pool/main/h/hello/hello_2.10-2.dsc \
           http://deb.debian.org/debian/pool/main/h/hello/hello_2.10.orig.tar.gz \
           http://deb.debian.org/debian/pool/main/h/hello/hello_2.10-2.debian.tar.xz

#. Create the artifact

   .. code-block:: console

    $ debusine create-artifact Test --workspace System --upload *
    result: success
    message: New artifact created in http://localhost:8011/api in workspace System with id 62.
    artifact_id: 62
    files_to_upload: 3

#. Create the file indicating the artifact_id that is going to be used:

   .. code-block:: console

    $ cat > work-request-hello.debusine << END
    build_components:
    - any
    - all
    distribution: stable
    host_architecture: amd64
    input:
      artifact_id: 62
    END

#. Create the work-request

   .. code-block:: console

    $ debusine create-work-request sbuild < work-request-hello.debusine
    result: success
    message: Work request registered on http://localhost:8011/api with id 53.
    work_request_id: 53

#. Check the status:

   .. code-block:: console

    $ debusine show-work-request 53

.. _creating-work-request-with-notification:
Creating a work request with a notification
-------------------------------------------

For the following example to work, the Debusine administrator should
have created a channel notification `lts`. Refer to the
:ref:`creating channel to send emails section <creating-channel-to-send-emails>`.

   .. code-block:: console
    :emphasize-lines: 9-13

    $ debusine create-work-request sbuild << END
    build_components:
    - any
    - all
    distribution: stable
    host_architecture: amd64
    input:
        source_package_url: https://deb.debian.org/debian/pool/main/h/hello/hello_2.10-2.dsc
    notifications:
        on_failure:
            - channel: lts
              to: ["lts@example.com"]
              subject: "Work request {work_request_id}: result: {work_request_result}"
    END

In this case, the notification channel default To: email is replaced by
"lts@example.com" (it could contain multiple emails) and the subject
will be "Work request {work_request_id}: result: {work_request_result}".

In the subject, the strings ``{work_request_id}`` and ``{work_request_result}``
are replaced by their values.

The possible fields for email notifications are:

- `from`: a default is provided by the debusine admin
- `to`: a default is provided by the debusine admin
- `subject`: a default is provided by the debusine admin
- `cc`: list of emails


Output of the ``debusine`` command
----------------------------------
If the ``debusine`` command succeeds, it prints relevant information to the
standard output in YAML format.

If an error occurs, the error messages will be printed to the standard error.

Return values
-------------

Return values of the ``debusine`` command:

===============  ==================================================================================
  Return value    Meaning
===============  ==================================================================================
 0                Success
 1                Error: unhandled exception. Please report the error
 2                Error: wrong arguments and options
 3                Error: any other type of error such as connection to the server is not possible,

                  invalid configuration file, etc.
===============  ==================================================================================

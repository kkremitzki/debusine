Welcome to Debusine's documentation!
====================================

debusine is a general purpose software factory tailored to the needs
of a Debian-based distribution. It brings together:

* an artifact storage system
* a task scheduler
* a workflow management system

.. toctree::
   :caption: Documentation for Administrators:

   admin/environment
   admin/debusine-server
   admin/debusine-worker
   admin/first-steps

.. toctree::
   :caption: Documentation for Users:

   user/overview
   user/debusine-client

.. toctree::
   :caption: Documentation for Developers:

   devel/why
   design/index
   devel/contributing
   devel/team-practices
   devel/coding-practices

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`

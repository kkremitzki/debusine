# Copyright 2021 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""
Task to build Debian packages with sbuild.

This task module implements the PackageBuild ontology for its task_data:
https://freexian-team.pages.debian.net/debusine/design/ontology.html#task-packagebuild
"""

import subprocess
from pathlib import Path
from typing import Optional

import debian.deb822 as deb822

import debusine.utils
from debusine.artifacts import (
    BinaryPackages,
    BinaryUpload,
    PackageBuildLog,
)
from debusine.client.models import RemoteArtifact
from debusine.tasks import Task, TaskConfigError
from debusine.tasks._task_mixins import (
    FetchBuildUploadMixin,
    TaskRunCommandMixin,
)
from debusine.tasks.sbuild_validator_mixin import SbuildValidatorMixin
from debusine.utils import read_dsc


class Sbuild(
    FetchBuildUploadMixin, TaskRunCommandMixin, SbuildValidatorMixin, Task
):
    """Task implementing a Debian package build with sbuild."""

    TASK_VERSION = 1
    TASK_DATA_SCHEMA = {
        "type": "object",
        "properties": {
            "input": {
                "type": "object",
                "properties": {
                    "source_package_url": {
                        "type": "string",
                    },
                    "artifact_id": {
                        "type": "integer",
                    },
                },
                "oneOf": [
                    {
                        "required": ["source_package_url"],
                    },
                    {
                        "required": ["artifact_id"],
                    },
                ],
            },
            "distribution": {
                "type": "string",
            },
            "host_architecture": {
                "type": "string",
            },
            "build_components": {
                "type": "array",
                "items": {
                    "enum": ["any", "all", "source"],
                },
                "uniqueItems": True,
            },
            "sbuild_options": {
                "type": "array",
                "items": {"type": "string"},
            },
        },
        "required": ["input", "distribution", "host_architecture"],
        "additionalProperties": False,
    }

    def __init__(self):
        """Initialize the sbuild task."""
        super().__init__()
        self.chroots = None
        self.builder = "sbuild"

        # sbuild package to build (PACKAGE.dsc parameter in sbuild command)
        # It can be a http(s) URL or a .dsc local file (POSIX path).
        self._sbuild_package: Optional[str] = None

        # if Task downloads a source artifact: the id of the source artifact
        # (task might use source_package_url or artifact_id)
        self._source_artifact_id: Optional[int] = None

        # if Task downloads a source artifact: the workspace of the artifact
        self._source_workspace: Optional[str] = None

        # Workspace is used when uploading artifacts.
        # If Sbuild.input is artifact_id: self._workspace will be set
        # to the workspace of the input artifact. If it's None:
        # it is not sent to the server, the server assigns it dynamically.
        self._workspace: Optional[str] = None

        # dsc_file Path. It might be set from self.configure_for_build
        # if using input.source_artifact_id. If it's using
        # input.source_package_url it is downloaded by "sbuild" and found
        # in the upload step.
        self._dsc_file: Optional[Path] = None

        # Files that might appear in .changes that have been already
        # uploaded.
        # When the BinaryUpload is created: it contains .changes file and
        # all the files listed in .changes excluding the files that were
        # already uploaded. Add the files that might be in .changes
        # and are uploaded in some other artifact other than BinaryUpload
        self._packaged_files: set[Path] = set()

    @property
    def chroot_name(self) -> str:
        """Build name of required chroot."""
        return "%s-%s" % (
            self.data["distribution"],
            self.data["host_architecture"],
        )

    @staticmethod
    def _call_dpkg_architecture():  # pragma: no cover
        return (
            subprocess.check_output(["dpkg", "--print-architecture"])
            .decode("utf-8")
            .strip()
        )

    def analyze_worker(self):
        """Report metadata for this task on this worker."""
        metadata = super().analyze_worker()

        self._update_chroots_list()
        chroots_key = self.prefix_with_task_name("chroots")
        metadata[chroots_key] = self.chroots.copy()

        host_arch_key = self.prefix_with_task_name("host_architecture")
        metadata[host_arch_key] = self._call_dpkg_architecture()

        return metadata

    def can_run_on(self, worker_metadata: dict) -> bool:
        """Check the specified worker can run the requested task."""
        if not super().can_run_on(worker_metadata):
            return False

        chroot_key = self.prefix_with_task_name("chroots")
        if self.chroot_name not in worker_metadata.get(chroot_key, []):
            return False

        return True

    @staticmethod
    def _call_schroot_list():  # pragma: no cover
        return (
            subprocess.check_output(["schroot", "--list"])
            .decode("utf-8")
            .strip()
        )

    def _update_chroots_list(self):
        """
        Provide support for finding available chroots.

        Ensure that aliases are supported as the DSC may explicitly refer to
        <codename>-security (or -backports) etc.

        Populates the self.chroots list, if the list is empty.
        No return value, this is a find, not a get.
        """
        if self.chroots is not None:
            return
        self.chroots = []
        output = self._call_schroot_list()
        for line in output.split("\n"):
            if line.startswith("chroot:") and line.endswith("-sbuild"):
                self.chroots.append(line[7:-7])

    def _verify_distribution(self):
        """Verify a suitable schroot exists."""
        self._update_chroots_list()

        if not self.chroots:
            self.logger.error("No sbuild chroots found")
            return False

        if self.chroot_name in self.chroots:
            return True

        self.logger.error("No suitable chroot found for %s", self.chroot_name)
        return False

    def configure(self, task_data):
        """Handle sbuild-specific configuration."""
        super().configure(task_data)

        # Handle default values
        self.data.setdefault("build_components", ["any"])
        self.data.setdefault("sbuild_options", [])

    def _cmdline(self):
        """Build the sbuild command line (idempotent)."""
        cmd = [
            self.builder,
            "--no-clean",
        ]
        if "any" in self.data["build_components"]:
            cmd.append("--arch-any")
        else:
            cmd.append("--no-arch-any")
        if "all" in self.data["build_components"]:
            cmd.append("--arch-all")
        else:
            cmd.append("--no-arch-all")
        if "source" in self.data["build_components"]:
            cmd.append("--source")
        else:
            cmd.append("--no-source")
        cmd.append("--dist=" + self.data["distribution"])
        cmd.append("--arch=" + self.data["host_architecture"])
        cmd.extend(self.data["sbuild_options"])
        cmd.append(self._sbuild_package)

        return cmd

    def execute(self) -> bool:
        """
        Verify task can be executed and super().execute().

        :raises: TaskConfigError.
        """  # noqa: D402
        if not self._verify_distribution():
            raise TaskConfigError(
                "No suitable schroot for %s-%s"
                % (self.data["distribution"], self.data["host_architecture"])
            )

        return super().execute()

    def _upload_package_build_log(
        self, build_directory: Path, source: str, version: str
    ) -> RemoteArtifact:
        assert self.debusine

        package_build_log = PackageBuildLog.create(
            source=source,
            version=version,
            file=debusine.utils.find_file_suffixes(build_directory, [".build"]),
        )
        return self.debusine.upload_artifact(
            package_build_log,
            workspace=self._workspace,
            work_request=self.work_request,
        )

    def _upload_binary_upload(self, build_directory: Path) -> RemoteArtifact:
        assert self.debusine

        changes_path = debusine.utils.find_file_suffixes(
            build_directory, [".changes"]
        )

        artifact_binary_upload = BinaryUpload.create(
            changes_file=changes_path, exclude_files=self._packaged_files
        )
        return self.debusine.upload_artifact(
            artifact_binary_upload,
            workspace=self._workspace,
            work_request=self.work_request,
        )

    def _create_binary_package_local_artifact(
        self, build_directory: Path, dsc: deb822.Dsc, suffixes: list[str]
    ) -> BinaryPackages:
        deb_paths = debusine.utils.find_files_suffixes(
            build_directory, suffixes
        )

        # Add files in self._packaged_files. When creating the BinaryUpload
        # these files will not be uploaded
        self._packaged_files.update(deb_paths)

        return BinaryPackages.create(
            srcpkg_name=dsc["source"],
            srcpkg_version=dsc["version"],
            version=dsc["version"],
            architecture=dsc["architecture"],
            files=deb_paths,
            packages=[],
        )

    def _upload_binary_packages(
        self, build_directory: Path, dsc: deb822.Dsc
    ) -> list[RemoteArtifact]:
        r"""Upload \*.deb and \*.udeb files."""
        assert self.debusine

        host_arch = self.data["host_architecture"]

        packages = []

        if "any" in self.data["build_components"]:
            prefix = "_" + host_arch
            packages.append(
                self._create_binary_package_local_artifact(
                    build_directory, dsc, [prefix + ".deb", prefix + ".udeb"]
                )
            )

        if "all" in self.data["build_components"]:
            prefix = "_all"
            packages.append(
                self._create_binary_package_local_artifact(
                    build_directory, dsc, [prefix + ".deb", prefix + ".udeb"]
                )
            )

        remote_artifacts: list[RemoteArtifact] = []
        for package in packages:
            if package.files:
                remote_artifacts.append(
                    self.debusine.upload_artifact(
                        package,
                        workspace=self._workspace,
                        work_request=self.work_request,
                    )
                )

        return remote_artifacts

    def _create_remote_binary_packages_relations(
        self,
        remote_build_log: RemoteArtifact,
        remote_binary_upload: RemoteArtifact,
        remote_binary_packages: list[RemoteArtifact],
    ):
        assert self.debusine

        for remote_binary_package in remote_binary_packages:
            if self._source_artifact_id is not None:
                self.debusine.relation_create(
                    remote_binary_package.id,
                    self._source_artifact_id,
                    "built-using",
                )

            self.debusine.relation_create(
                remote_build_log.id, remote_binary_package.id, "relates-to"
            )

            self.debusine.relation_create(
                remote_binary_upload.id,
                remote_binary_package.id,
                "extends",
            )
            self.debusine.relation_create(
                remote_binary_upload.id,
                remote_binary_package.id,
                "relates-to",
            )

    def fetch(self, build_directory: Path):
        """
        Fetch the required (if any) artifact for the build.

        If self.data["input"]["artifact_id"] contains the artifact_id:
        downloads it and set self._source_workspace to the artifact's workspace.

        Save it into build_directory.
        """
        assert self.debusine

        data_input = self.data["input"]

        if (artifact_id := data_input.get("artifact_id")) is not None:
            artifact_response = self.debusine.download_artifact(
                artifact_id, build_directory, tarball=False
            )
            self._source_workspace = artifact_response.workspace

    def configure_for_build(self, download_directory: Path) -> bool:
        """
        Configure Task: set variables needed for the build() step.

        Return True if configuration worked, False, if there was a problem.

        Write to a log file if a problem happened.
        """
        data_input = self.data["input"]

        if (url := data_input.get("source_package_url")) is not None:
            self._sbuild_package = url
        else:
            self._source_artifact_id = data_input["artifact_id"]
            dsc_files = debusine.utils.find_files_suffixes(
                download_directory, [".dsc"]
            )

            if len(dsc_files) != 1:
                config_for_build_file = self.create_debug_log_file(
                    "configure_for_build.log"
                )
                list_of_files = sorted(map(str, download_directory.glob("*")))
                config_for_build_file.write(
                    f"There must be one *.dsc file. "
                    f"Current files: {list_of_files}"
                )
                config_for_build_file.close()

                return False

            self._workspace = self._source_workspace
            self._dsc_file = dsc_files[0]
            self._sbuild_package = str(self._dsc_file)

        return True

    def upload_artifacts(self, build_directory: Path, *, build_success: bool):
        """
        Upload the artifacts from build_directory.

        :param build_directory: build directory containing the files that
          will be uploaded.
        :param build_success: if False skip uploading .changes and *.deb/*.udeb
        """
        assert self.debusine

        dsc_file = debusine.utils.find_dsc(self._dsc_file, build_directory)

        dsc = read_dsc(dsc_file)

        if dsc is not None:
            # Upload the .build file (PackageBuildLog)
            remote_build_log = self._upload_package_build_log(
                build_directory, dsc["source"], dsc["version"]
            )

            if self._source_artifact_id:
                self.debusine.relation_create(
                    remote_build_log.id, self._source_artifact_id, "relates-to"
                )

            if build_success:
                # Upload the *.deb/*.udeb files (BinaryPackages)
                remote_binary_packages = self._upload_binary_packages(
                    build_directory, dsc
                )

                # Upload the .changes and the rest of the files
                remote_binary_changes = self._upload_binary_upload(
                    build_directory
                )

                # Create the relations
                self._create_remote_binary_packages_relations(
                    remote_build_log,
                    remote_binary_changes,
                    remote_binary_packages,
                )

# Copyright 2021-2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Consumers for the server application."""

import asyncio
import json
import logging
from datetime import datetime
from typing import Optional, Type
from urllib.parse import parse_qsl

from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer

from rest_framework.fields import DateTimeField
from rest_framework.permissions import BasePermission
from rest_framework.renderers import JSONRenderer

from debusine.db.models import WorkRequest, Worker, Workspace
from debusine.server.serializers import OnWorkRequestCompleted
from debusine.server.views import (
    IsTokenUserAuthenticated,
    IsWorkerAuthenticated,
)

logger = logging.getLogger(__name__)


class RequestAdaptor:
    """Minimal class used when helper classes expect a Request."""

    def __init__(self, token: Optional[str]):
        """Initialize RequestAdaptor."""
        self.headers = {}

        if token is not None:
            self.headers["token"] = token


class ConsumerMixin:
    """Mixin used for different consumer classes."""

    def get_header_value(self, header_field_name: str) -> Optional[str]:
        """
        Return the value of header_field_name from self.scope['headers'].

        :param header_field_name: case-insensitive, utf-8 encoded.
        :return: None if header_field_name is not found in
          self.scope['headers'] or the header's content.
        """
        encoded_header_field_name = header_field_name.lower().encode('utf-8')

        for name, value in self.scope['headers']:
            if name == encoded_header_field_name:
                return value.decode('utf-8')

        return None

    async def reject_connection(self, reason):
        """Send JSON rejecting the connection and logs it."""
        msg = {
            'type': 'connection_rejected',
            'reason': reason,
        }

        await self.send(text_data=json.dumps(msg), close=True)

        logger.info("Consumer rejected. %s", reason)

    async def has_permission(
        self, permission_class: Type[BasePermission]
    ) -> bool:
        """Return bool checking if the request has permission to connect."""
        token_header: str = self.get_header_value("token")
        request_adaptor = RequestAdaptor(token_header)

        return await database_sync_to_async(permission_class().has_permission)(
            request_adaptor, None
        )


class WorkerConsumer(ConsumerMixin, AsyncWebsocketConsumer):
    """
    Implement server-side of a Worker.

    After the client worker connects to the server: WorkerConsumer
    requests information, send tasks, etc.
    """

    # Call self._send_dynamic_metadata_request() every
    # REQUEST_DYNAMIC_METADATA_SECONDS
    REQUEST_DYNAMIC_METADATA_SECONDS = 3600

    def __init__(self, *args, **kwargs):
        """Initialise WorkerConsumer member variables."""
        super().__init__(*args, **kwargs)
        self._worker: Optional[Worker] = None
        self._request_dynamic_metadata_task = None

    async def _channel_layer_group_add(self, token_key: str, channel_name: str):
        await self.channel_layer.group_add(token_key, channel_name)

    async def connect(self):
        """Worker client is connecting."""
        await self.accept()

        if not await self.has_permission(IsWorkerAuthenticated):
            await self.reject_connection(
                "No token header, token not associated to a worker "
                "or not enabled"
            )
            return

        token_header: str = self.get_header_value("token")

        self._worker = await database_sync_to_async(
            Worker.objects.select_related("token").get
        )(token__key=token_header)

        try:
            await self._channel_layer_group_add(
                self._worker.token.key, self.channel_name
            )
        except Exception as exc:
            logger.error(  # noqa: G200
                'Error adding worker to group (Redis): %s', exc
            )
            await self.reject_connection('Service unavailable')
            return False

        logger.info("Worker connected: %s", self._worker.name)

        await database_sync_to_async(self._worker.mark_connected)()

        self._request_dynamic_metadata_task = asyncio.create_task(
            self._dynamic_metadata_refresher(),
            name='dynamic_metadata_refresher',
        )

        # If there are WorkRequests running or pending: send to the worker
        # "work-request-available" message
        if await database_sync_to_async(
            WorkRequest.objects.running_or_pending_exists
        )(self._worker):
            await self._send_work_request_available()

    async def _send_work_request_available(self):
        await self.send(
            text_data=json.dumps(
                {
                    "type": "websocket.send",
                    "text": "work_request_available",
                }
            )
        )

    async def _dynamic_metadata_refresher(self):
        while True:
            await self._send_dynamic_metadata_request()
            await asyncio.sleep(self.REQUEST_DYNAMIC_METADATA_SECONDS)

    async def _send_dynamic_metadata_request(self):
        await self.send(
            text_data=json.dumps(
                {
                    "type": "websocket.send",
                    "text": "request_dynamic_metadata",
                }
            )
        )

    async def disconnect(self, close_code):
        """Worker has disconnected. Cancel tasks, mark as disconnect, etc."""
        if self._request_dynamic_metadata_task:
            self._request_dynamic_metadata_task.cancel()

        if self._worker:
            await self.channel_layer.group_discard(
                self._worker.token.key, self.channel_name
            )

            await database_sync_to_async(self._worker.mark_disconnected)()
            logger.info(
                "Worker disconnected: %s (code: %s)",
                self._worker.name,
                close_code,
            )

    async def worker_disabled(self, event):  # noqa: U100
        """Worker has been disabled. Send a connection_closed msg."""
        logger.info("Worker %s disabled", self._worker.name)

        msg = {
            "type": "connection_closed",
            "reason": 'Token has been disabled '
            f'(token: "{self._worker.token.key}")',
            "reason_code": "TOKEN_DISABLED",
        }
        await self.send(text_data=json.dumps(msg), close=True)

    async def work_request_assigned(self, event):  # noqa: U100
        """Work Request has been assigned to the worker. Send channel msg."""
        await self._send_work_request_available()


class OnWorkRequestCompletedConsumer(ConsumerMixin, AsyncWebsocketConsumer):
    """Send work_request_completed notifications."""

    def __init__(self, *args, **kwargs):
        """Initialise OnWorkRequestCompletedConsumer."""
        super().__init__(*args, **kwargs)

        # If _workspaces is not None: list of the workspaces that are being
        # monitored, otherwise all the workspaces that the client has access to
        self._workspaces_only: Optional[list[int]] = None

        self._query_string: dict = {}

    async def _get_workspaces_from_query_params(self):
        workspace_names = self._query_string.get("workspaces")

        if workspace_names is None:
            return

        workspace_names = workspace_names.split(",")

        workspace_ids = set()

        for workspace_name in workspace_names:
            # TODO: check that the current user has access to this
            # workspace (need implementing #80)
            try:
                workspace = await database_sync_to_async(Workspace.objects.get)(
                    name=workspace_name
                )
            except Workspace.DoesNotExist:
                raise Workspace.DoesNotExist(
                    f'Workspace "{workspace_name}" not found'
                )
            workspace_ids.add(workspace.id)

        return workspace_ids

    async def connect(self):
        """Client is connecting."""
        self._query_string = dict(
            parse_qsl(self.scope["query_string"].decode("utf-8"))
        )

        await self.accept()

        if not await self.has_permission(IsTokenUserAuthenticated):
            await self.reject_connection(
                "No token header, token not associated to a user "
                "or not enabled"
            )
            return

        try:
            self._workspaces_only = (
                await self._get_workspaces_from_query_params()
            )
        except Workspace.DoesNotExist as exc:
            await self.reject_connection(str(exc))
            return

        await self.channel_layer.group_add(
            "work_request_completed", self.channel_name
        )

        await self._send_pending_work_requests_since_last_completed()

    @database_sync_to_async
    def _get_pending_work_requests(
        self, completed_at_since: datetime
    ) -> list[WorkRequest]:
        """
        Return WorkRequests that completed_at on or after completed_at_since.

        If there is one WorkRequest that completed_at == completed_at_since:
        do not include this one (the client was already notified).

        If more than one WorkRequest completed_at == completed_at_since:
        include both of them. The client might have not processed all of them.
        """
        on_last_completed_count = WorkRequest.objects.filter(
            completed_at=completed_at_since
        ).count()

        if on_last_completed_count == 1:
            filter_kwargs = {"completed_at__gt": completed_at_since}
        else:
            filter_kwargs = {"completed_at__gte": completed_at_since}

        return list(
            WorkRequest.objects.filter(**filter_kwargs).order_by("completed_at")
        )

    async def _send_pending_work_requests_since_last_completed(self) -> None:
        completed_at_since = self._query_string.get("completed_at_since")

        if completed_at_since is None:
            return

        completed_at_since = DateTimeField().to_internal_value(
            completed_at_since
        )

        for work_request in await self._get_pending_work_requests(
            completed_at_since
        ):
            await self._send_work_request_completed(
                work_request.id,
                work_request.workspace_id,
                work_request.completed_at.isoformat(),
                work_request.result,
            )

    async def work_request_completed(self, event):
        """Work Request has completed."""
        work_request_id = event["work_request_id"]
        work_space_id = event["workspace_id"]
        completed_at = event["completed_at"]
        result = event["result"]

        await self._send_work_request_completed(
            work_request_id, work_space_id, completed_at, result
        )

    async def _send_work_request_completed(
        self,
        work_request_id: int,
        workspace_id: int,
        completed_at: str,
        result: str,
    ) -> None:
        if (
            self._workspaces_only is not None
            and workspace_id not in self._workspaces_only
        ):
            return

        serializer = OnWorkRequestCompleted(
            data={
                "type": "websocket.send",
                "text": "work_request_completed",
                "work_request_id": work_request_id,
                "completed_at": datetime.fromisoformat(completed_at),
                "result": result,
            }
        )

        serializer.is_valid(raise_exception=True)

        await self.send(
            text_data=JSONRenderer().render(serializer.data).decode("utf-8")
        )

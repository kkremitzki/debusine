### User story

_In this section, you describe the new feature from the point of view of
a debusine user (it can be the administrator, an authenticated user,
an anonymous user accessing the web interface, …)._

### Implementation plan

_Fill in this section with a rough plan of how the feature will be
implemented in the code base. This does not have to be filled from
the start but should be filled before writing any code. For larger
changes, it might be wise to first write official design documentation.
In those cases, you should put here links to the corresponding
documentation once it has been merged._

### Notes

This issue description can be modified to integrate feedback from
comments and from associated merge requests.

/label ~"User story"

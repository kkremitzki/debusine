.. _add_repository:

.. code-block:: console

  $ curl -s https://freexian-team.pages.debian.net/debusine/repository/public-key.asc \
    | sudo tee /etc/apt/trusted.gpg.d/debusine.asc
  $ echo "deb [arch=all] https://freexian-team.pages.debian.net/debusine/repository/ bookworm main" \
    | sudo tee /etc/apt/sources.list.d/debusine.list
  $ sudo apt update
  $ sudo apt install debusine-server

To set up ``bullseye-backports`` if needed:

.. code-block:: console

  # Only needed on Bullseye
  $ echo "deb http://deb.debian.org/debian bullseye-backports main" | sudo tee /etc/apt/sources.list.d/bullseye-backports.list
  $ sudo apt update
  $ sudo apt install python3-django/bullseye-backports

# Copyright 2022 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""
Debusine tasks.

Import this module using for example:
from debusine.tasks import Task, TaskConfigError

Debusine tasks such as sbuild become available
"""

import sys

from ._task import Task, TaskConfigError

# Sub-tasks need to be imported in order to be available to Task
# (e.g. for Task.is_valid_task_name). They are registered via
# Task.__init_subclass__
from .noop import Noop  # noqa: F401, I202
from .sbuild import Sbuild  # noqa: F401, I202

# Import the documentation from where the code lives
__doc__ = sys.modules[Task.__module__].__doc__

__all__ = [
    'Task',
    'TaskConfigError',
]

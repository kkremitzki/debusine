# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""debusine Homepage URL Configuration."""
from django.contrib.auth import views as auth_views
from django.urls import path

from debusine.web.views import LoginView

app_name = "accounts"

urlpatterns = [
    path(
        "accounts/login/",
        LoginView.as_view(template_name="account/login.html"),
        name="login",
    ),
    path(
        "accounts/logout/",
        auth_views.LogoutView.as_view(template_name="account/logged_out.html"),
        name="logout",
    ),
]

# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""
Local artifact's representation.

See docs/design/ontology.html for the semantics of the Artifacts.
"""
from pathlib import Path
from typing import Any, Iterable, Optional, Sequence, Type

import debian.deb822 as deb822

import pydantic
from pydantic import validate_model

from debusine import utils
from debusine.artifacts.utils import files_in_meta_file_match_files
from debusine.client.models import (
    ArtifactCreateRequest,
    FileModel,
    FilesType,
    StrictBaseModel,
)


class LocalArtifact(StrictBaseModel):
    """Represent an artifact locally."""

    category: str

    # Keys are paths in the artifact. Values the paths in the local system
    files: dict[str, Path] = {}
    data: dict = {}

    _local_artifacts_category_to_class: dict[str, Type["LocalArtifact"]] = {}

    def __init_subclass__(cls, **kwargs):
        """
        Register subclass into LocalArtifact._local_artifacts_category_to_class.

        Allow to list possible valid options (in the client or server).
        """
        super().__init_subclass__(**kwargs)

        LocalArtifact._local_artifacts_category_to_class[cls._category] = cls

    @staticmethod
    def artifact_categories() -> list[str]:
        """Return list of artifact categories."""
        return list(LocalArtifact._local_artifacts_category_to_class.keys())

    @staticmethod
    def class_from_category(category: str) -> Type["LocalArtifact"]:
        """Return class sub_local_artifact."""
        return LocalArtifact._local_artifacts_category_to_class[category]

    @classmethod
    def _create(cls) -> Any:
        # Python 3.11: use typing.Self instead of Any
        return cls(category=cls._category)

    def add_local_file(
        self,
        file: Path,
        *,
        artifact_base_dir: Optional[Path] = None,
    ):
        """
        Add a local file in the artifact.

        :param file: file in the local file system that is added
           to the artifact
        :param artifact_base_dir: base directory of the artifact. Must be
           an absolute path.
           If it's None: file is added in the root of the artifact.
           If it's not None: file is added with the relative path of the
           file with the artifact_base_dir. E.g.
           file=/tmp/artifact/dir1/file1
           artifact_base_dir=/tmp/artifact
           Path of this file in the artifact: dir1/file1
        :raises ValueError: artifact_base_dir is not absolute or is not
          a directory; file does not exist; the path in the artifact already
          had a file.
        """
        if artifact_base_dir is not None:
            if not artifact_base_dir.is_absolute():
                raise ValueError(f'"{artifact_base_dir}" must be absolute')
            if not artifact_base_dir.is_dir():
                raise ValueError(
                    f'"{artifact_base_dir}" does not exist or '
                    f'is not a directory'
                )

            if not file.is_absolute():
                file = artifact_base_dir.joinpath(file)

            path_in_artifact = file.relative_to(artifact_base_dir).as_posix()
        else:
            path_in_artifact = file.name

        if not file.exists():
            raise ValueError(f'"{file}" does not exist')

        if not file.is_file():
            raise ValueError(f'"{file}" is not a file')

        file_absolute = file.absolute()

        if path_in_artifact in self.files:
            raise ValueError(
                f"File with the same path ({path_in_artifact}) "
                f"is already in the artifact "
                f'("{self.files[path_in_artifact]}" and "{file_absolute}")'
            )

        self.files[path_in_artifact] = file_absolute

    def validate_model(self):
        """Raise ValueError with an error if the model is not valid."""
        *_, error = validate_model(self.__class__, self.__dict__)

        if error is not None:
            raise ValueError(f"Model validation failed: {error}")

    def serialize_for_create_artifact(
        self, *, workspace: Optional[str], work_request: Optional[int] = None
    ) -> dict:
        """Return dictionary to be used by the API to create an artifact."""
        files: dict[str, FileModel] = {}
        for artifact_path, local_path in self.files.items():
            files[artifact_path] = FileModel.create_from(local_path)

        self.validate_model()

        serialized = ArtifactCreateRequest(
            workspace=workspace,
            category=self.category,
            files=files,
            data=self.data,
            work_request=work_request,
        ).dict()

        # If the workspace was not specified: do not send it to the API.
        # The server will assign it.
        if serialized["workspace"] is None:
            del serialized["workspace"]

        return serialized

    @classmethod
    def _validate_files_length(
        cls, files: FilesType, number_of_files: int
    ) -> FilesType:  # noqa: U100
        """Raise ValueError if number of files is not number_of_files."""
        if (actual_number_files := len(files)) != number_of_files:
            raise ValueError(
                f"Expected number of files: {number_of_files} "
                f"Actual: {actual_number_files}"
            )
        return files

    @classmethod
    def _validate_files_end_in(
        cls, files: FilesType, suffixes: Sequence[str]
    ) -> FilesType:  # noqa: U100
        """Raise ValueError if any file does not end in one of suffixes."""
        for file_name in files.keys():
            if not file_name.endswith(tuple(suffixes)):
                raise ValueError(
                    f'Valid file suffixes: {suffixes}. '
                    f'Invalid filename: "{file_name}"'
                )
        return files


class WorkRequestDebugLogs(LocalArtifact):
    """
    WorkRequestDebugLogs: help debugging issues executing the task.

    Log files for debusine users in order to debug possible problems in their
    WorkRequests.
    """

    _category = "debusine:work-request-debug-logs"

    category: str

    @classmethod
    def create(cls, *, files: Iterable[Path]) -> "WorkRequestDebugLogs":
        """Return a WorkRequestDebugLogs."""
        package = super()._create()

        for file in files:
            package.add_local_file(file)

        return package


class PackageBuildLog(LocalArtifact):
    """PackageBuildLog: represents a build log file."""

    _category = "debian:package-build-log"

    @classmethod
    def create(
        cls,
        *,
        file: Path,
        source: str,
        version: str,
    ) -> "PackageBuildLog":
        """Return a PackageBuildLog."""
        package = super()._create()

        package.add_local_file(file)

        package.data["source"] = source
        package.data["version"] = version

        return package

    @pydantic.validator("files")
    def validate_files_length_is_one(
        cls, files: FilesType  # noqa: U100
    ) -> FilesType:
        """Validate that artifact has only one file."""
        return super()._validate_files_length(files, 1)

    @pydantic.validator("files")
    def file_must_end_in_build(
        cls, files: FilesType  # noqa: U100
    ) -> FilesType:
        """Raise ValueError if the file does not end in .build."""
        return super()._validate_files_end_in(files, [".build"])


def deb822dict_to_dict(element):
    """
    Traverse recursively element converting Deb822Dict to dict.

    deb822.Changes() return a Deb822Dict with some other inner-elements
    being Deb822Dict. This function traverse it and converts any elements
    being a Deb822Dict to a Python dict.

    Reason is to simplify is that json module cannot encode Deb822Dict. To
    simplify let's convert Deb822Dict as soon as possible to dict.
    """
    if isinstance(element, (dict, deb822.Deb822Dict)):
        result = {}
        for key, value in element.items():
            result[key] = deb822dict_to_dict(value)
    elif isinstance(element, list):
        result = []
        for item in element:
            result.append(deb822dict_to_dict(item))
    else:
        result = element

    return result


class BinaryUpload(LocalArtifact):
    """BinaryUpload: encapsulate a .changes and files listed in it."""

    _category = "debian:binary-upload"
    _type = "dpkg"

    @classmethod
    def create(
        cls,
        *,
        changes_file: Path,
        exclude_files: set[Path],
    ) -> "BinaryUpload":
        """
        Return a BinaryUpload. Add the changes_files and files listed in it.

        :param changes_file: a .changes file. Parsed by deb822.Changes.
        :param exclude_files: do not add them in files even if listed in the
          Files section in the changes_file.
        """
        package = super()._create()

        package.add_local_file(changes_file)

        package.data["type"] = cls._type

        with changes_file.open() as changes_obj:
            package.data["changes-fields"] = deb822dict_to_dict(
                deb822.Changes(changes_obj)
            )

        # Add any files referenced by .changes (excluding the exclude_files)
        base_directory = changes_file.parent
        for file in package.data["changes-fields"].get("Files", []):
            file = base_directory / file["name"]

            if file not in exclude_files:
                package.add_local_file(file)

        return package

    @pydantic.validator("data")
    def type_must_be_in_data(cls, data):  # noqa: U100
        """Raise ValueError if no 'type' in data."""
        if "type" not in data:
            raise ValueError('Missing mandatory field in data: "type"')

        return data

    @pydantic.validator("files")
    def files_contain_changes(cls, files):  # noqa: U100
        """Raise ValueError files does not have a .changes file."""
        for file in files.keys():
            if Path(file).suffix == ".changes":
                return files

        raise ValueError(f"No .changes in {sorted(files.keys())}")

    @pydantic.validator("files")
    def files_contains_files_in_changes(
        cls, files: dict[str, Path]  # noqa: U100
    ):
        """
        Validate that set(files) == set(files_in_dsc_file).

        Exception: The .dsc file must be in files but not in the .dsc file.
        """
        return files_in_meta_file_match_files(
            ".changes",
            deb822.Changes,
            files,
            ignore_suffixes_metadata=(".deb", ".udeb"),
        )


class SourcePackage(LocalArtifact):
    """SourcePackage: contains source code to be built into BinaryPackages."""

    _category = "debian:source-package"

    @classmethod
    def create(
        cls, *, name: str, version: str, files: list[Path]
    ) -> "SourcePackage":
        """Return a SourcePackage setting files and data."""
        package = super()._create()

        dsc_fields = {}
        for file in files:
            if file.suffix == ".dsc":
                dsc_fields = utils.read_dsc(file)

        package.data = {
            "name": name,
            "version": version,
            "type": "dpkg",
            "dsc-fields": dsc_fields,
        }

        for file in files:
            package.add_local_file(file)

        return package

    @pydantic.validator("files")
    def files_contain_one_dsc(cls, files: dict[str, Path]):  # noqa: U100
        """Raise ValueError files does not have a .dsc file."""
        dsc_count = 0
        for file in files.keys():
            if Path(file).suffix == ".dsc":
                dsc_count += 1

        if dsc_count != 1:
            raise ValueError(f"Must be one .dsc file. There are: {dsc_count}")

        return files

    @pydantic.validator("files")
    def files_contains_files_in_dsc(cls, files: dict[str, Path]):  # noqa: U100
        """
        Validate that set(files) == set(files_in_dsc_file).

        Exception: The .dsc file must be in files but not in the .dsc file.
        """
        return files_in_meta_file_match_files(".dsc", deb822.Dsc, files)


class BinaryPackages(LocalArtifact):
    r"""BinaryPackages: encapsulates \*.deb / \*.udeb."""

    _category = "debian:binary-packages"

    @classmethod
    def create(
        cls,
        *,
        srcpkg_name: str,
        srcpkg_version: str,
        version: str,
        architecture: str,
        packages: list[str],
        files: list[Path],
    ) -> "BinaryPackages":
        """Return a BinaryPackages setting files and data."""
        package = super()._create()

        package.data = {
            "srcpkg-name": srcpkg_name,
            "srcpkg-version": srcpkg_version,
            "version": version,
            "architecture": architecture,
            "packages": packages,  # binary packages part of the
            # build for architecture
        }

        for file in files:
            package.add_local_file(file)

        return package

    @pydantic.validator("files")
    def files_must_end_in_deb_or_udeb(
        cls, files: FilesType  # noqa: U100
    ) -> FilesType:
        """Raise ValueError if a file does not end in .deb or .udeb."""
        return super()._validate_files_end_in(files, [".deb", ".udeb"])

    @pydantic.validator("files")
    def files_more_than_zero(cls, files: FilesType) -> FilesType:  # noqa: U100
        """Raise ValueError if len(files) == 0."""
        if len(files) == 0:
            raise ValueError("Must have at least one file")

        return files

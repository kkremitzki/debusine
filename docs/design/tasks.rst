=====
Tasks
=====

External API: input and output of tasks
=======================================

Autopkgtest task (NOT IMPLEMENTED YET)
--------------------------------------

The ``task_data`` associated to this task can contain the following keys:

* ``input`` (required): a dictionary describing the input data:

  * ``source_arfifact_id`` (required): the ID of the artifact representing
    the source package to be tested with autopkgtest
  * ``binary_artifacts_ids`` (required): a list of ``debian:binary-packages``
    artifact IDs representing the binary packages to be tested with
    autopkgtest (they are expected to be part of the same source package
    as the one identified with ``source_artifact_id``)
  * ``context_artifacts_ids`` (optional): a list of ``debian:binary-packages``
    artifact IDs representing a special context for the tests. This is
    used to trigger autopkgtests of reverse dependencies, where
    ``context_artifacts_ids`` is set to the artifacts of the updated
    package whose reverse dependencies are tested, and source/binary artifact
    IDs are one of the reverse dependency whose autopkgtests will be
    executed.

* ``architecture`` (required): the Debian architecture that will be used
  in the chroot or VM where tests are going to be run.  The packages
  submitted in ``input:binary_artifacts_ids`` usually have a matching
  architecture (but need not in the case of cross-architecture package
  testing, eg. testing i386 packages in an amd64 system).

* ``distribution`` (required): base distribution of the
  chroot/container/VM that will be used to run the tests. The distribution
  codename is prefixed by the vendor identifier.

* ``backend`` (optional): the virtualization backend to use, defaults to
  ``auto`` where the task is free to use the most suitable backend. Other
  valid options are ``schroot``, ``lxc``, ``qemu``, ``podman``.

.. note::

   We are not supporting all backends as there's a cost in supporting
   the required infrastructure setup to make use of all the backends. For
   schroot, we already have that due to the sbuild task. For ``podman``
   and ``qemu`` we can do all the setup without root rights. For ``lxc``
   it's the reference used by Debian and we want to support it too.

* ``include_tests`` (optional): a list of the tests that will be executed.
  If not provided (or empty), defaults to all tests being executed. Translates into
  ``--test-name=TEST`` command line options.

* ``exclude_tests`` (optional): a list of tests that will skipped.
  If not provided (or empty), then no tests are skipped. Translates into
  the ``--skip-test=TEST`` command line options.

* ``debug_level`` (optional, defaults to 0): a debug level between 0 and
  3. Translates into ``-d`` up to ``-ddd`` command line options.

* ``extra_apt_sources`` (optional): a list of APT sources. Each APT source
  is described by a single line (``deb http://MIRROR CODENAME
  COMPONENT``) that is copied to a file in /etc/apt/sources.list.d.
  Translates into ``--add-apt-source`` command line options.

* ``use_packages_from_base_repository`` (optional, defaults to False): if
  True, then we pass ``--apt-default-release=$DISTRIBUTION`` with the name
  of the base distribution given in the ``distribution`` key.

* ``environment`` (optional): a dictionary listing environment variables
  to inject in the build and test enviroment. Translates into
  (multiple) ``--env=VAR=VALUE`` command line options.

* ``needs_internet`` (optional, defaults to "run"): Translates directly
  into the ``--needs-internet`` command line option. Allowed values
  are "run", "try" and "skip".

* ``fail_on`` (optional): indicates whether the work request must be
  marked as failed in different scenario identified by the following
  sub-keys:

  * ``failed_test`` (optional, defaults to true): at least one test has
    failed (and the test was not marked as flaky).
  * ``flaky_test`` (optional, defaults to false): at least one flaky test
    has failed.
  * ``skipped_test`` (optional, defaults to false): at least one test has
    been skipped.

* ``timeout`` (optional): a dictionary where each key/value pair maps to
  the corresponding ``--timeout-KEY=VALUE`` command line option with the
  exception of the ``global`` key that maps to ``--timeout=VALUE``.
  Supported keys are ``global``, ``factor``, ``short``, ``install``, ``test``,
  ``copy`` and ``build``.

.. note::

   At this point, we have voluntarily not added any key for the
   ``--pin-packages`` option because that option is not explicit enough:
   differences between the mirror used to schedule jobs and the mirror
   used by the jobs result in tests that are not testing the version that
   we want. At this point, we believe it's better to submit all modified
   packages explicitly via ``input:context_artifacts_ids`` so that we are
   sure of the .deb that we are submitting and testing with. That way we
   can even test reverse dependencies before the modified package is
   available in any repository.

   This assumes that we can submit arbitrary .deb on the command line and
   that they are effectively used as part of the package setup.

autopkgtest is always run with the options ``--apt-upgrade
--output-dir=ARTIFACT-DIR --summary=ARTIFACT-DIR/summary``. An artifact
of category ``debian:autopkgtest`` is generated and its
content is a copy of what's available in the ``ARTIFACT-DIR`` (except
for .deb that have been built due to the ``build-needed`` restriction,
they are excluded to save space). The artifact has "relates to"
relationships for the artifacts used as input that are part of the
source package being tested.

The ``data`` field of the artifact has the following structure:

* ``results``: a dictionary with details about the tests that have been
  run. Each key is the name of the test (as shown in the summary file)
  and the value is another dictionary with the following keys:

  * ``status``: one of ``PASS``, ``FAIL``, ``FLAKY`` or ``SKIPPED``
  * ``details``: more details when available

* ``cmdline``: the complete command line that has been used for the run
* ``source_package``: a dictionnary with some information about the source
  package hosting the tests that have been run. It has the following
  sub-keys:

  * ``name``:the name of the source package
  * ``version``: the version of the source package
  * ``url``: the URL of the source package

* ``architecture``: the architecture of the system where tests have been
  run
* ``distribution``: the distribution of the system where tests have been
  run (formatted as ``VENDOR:CODENAME``)

Lintian task (NOT IMPLEMENTED YET)
----------------------------------

The ``task_data`` associated to this task can contain the following keys:

* ``input`` (required): a dictionary of values describing the input data,
  one of the sub-keys is required but both can be given at the same time
  too.

  * ``source_arfifact_id`` (optional): the ID of the artifact representing
    the source package to be tested with lintian
  * ``binary_artifacts_ids`` (optional): a list of ``debian:binary-packages``
    artifact IDs representing the binary packages to be tested with
    lintian (they are expected to be part of the same source package
    as the one identified with ``source_artifact_id``)

.. note::

   While it's possible to submit only a source or only a single binary
   artifact, you should aim to always submit source + arch-all + arch-any
   related artifacts to have the best test coverage as some tags can only
   be emitted when lintian has access to all of them at the same time.

* ``output`` (optional): a dictionary of values controlling some aspects
  of the generated artifacts

  * ``source_analysis`` (optional, defaults to True): indicates whether
    we want to generate the ``debian:lintian`` artifact for the source
    package

  * ``binary_all_analysis`` (optional, defaults to True): same as
    ``source_analysis`` but for the ``debian:lintian`` artifact related
    to ``Architecture: all`` packages

  * ``binary_any_analysis`` (optional, defaults to True): same as
    ``source_analysis`` but for the ``debian:lintian`` artifact related
    to ``Architecture: any`` packages

* ``target_distribution`` (optional): the fully qualified name of the
  distribution that will provide the lintian software to analyze the
  packages. Defaults to ``debian:unstable``.

* ``min_lintian_version`` (optional): request that the analysis be
  performed with a lintian version that is higher or equal to the version
  submitted. If a satisfying version is not pre-installed and cannot be
  installed with ``apt-get install lintian``, then the work request
  is aborted.

* ``include_tags`` (optional): a list of the lintian tags that are allowed to
  be reported. If not provided (or empty), defaults to all. Translates into the
  ``--tags`` or ``--tags-from file`` command line option.

* ``exclude_tags`` (optional): a list of the lintian tags that are not
  allowed to be reported. If not provided (or empty), then no tags are
  hidden. Translates into the ``--suppress-tags`` or
  ``--suppress-tags-from file`` command line option.

* ``fail_on_severity`` (optional, defaults to ``none``): if the analysis emits
  tags of that severity or higher, then the task will return a "failure"
  instead of a "success". Valid values are (in decreasing severity)
  "error", "warning", "info", "pedantic", "experimental", "overriden".
  "none" is a special value indicating that we should never fail.

The lintian runs will always use the options ``--display-level
">=classification" --display-experimental --info --show-overrides`` to
collect the full set of data that lintian can provide.

.. note::

   Current lintian can generate "masked" tags (with `M:` prefix) when you
   use ``--show-overrides``. For the purpose of debusine, we entirely
   ignore those tags on the basis that it's lintian's decision to hide
   them (and not the maintainer's decision) and as such, they don't bring
   any useful information. Lintian is full of exceptions to not emit some
   tags and the fact that some tags rely on a modular exception mechanism
   that can be diverted to generate masked tags is not useful to package
   maintainers.

   For those reasons, we suggested to lintian's maintainers to entirely
   stop emitting those tags in https://bugs.debian.org/1053892

Between 1 to 3 artifacts of category ``debian:lintian`` will be generated (one
for each source/binary package artifact submitted) and they will have a
"relates to" relationship with the corresponding artifact that has been
analyzed. These artifacts contain a ``lintian.txt`` file with the raw
(unfiltered) lintian output and an ``analysis.json`` file with the details
about all the tags discovered (in a top-level ``tags`` key), some
statistics/summary (in a top-level ``summary`` key) and a ``version`` key
with the value ``1.0`` if the content follows the (initial) JSON structure
described below.

The ``summary`` key is also duplicated in the ``data`` field of the
artifact. It is a dictionary with the following keys:

* ``tags_count_by_severity``: a dictionary with a sub-key for each of
  the possible severities documenting the number of tags of the
  corresponding severity that have been emitted by lintian
* ``package_filename``: a dictionary mapping the name of the
  binary or source package to its associated filename (will be a single
  key dictionary for the case of a source package lintian analysis, and a
  multiple keys one for the case of an analysis of binary packages)
* ``tags_found``: the list of tags that have been found during the analysis
* ``lintian_version``: the lintian version used for the analysis
* ``distribution``: the distribution in which lintian has been run

The ``tags`` key in analysis.json is a sorted list of tags where each tag
is represented with a dictionary. The list is sorted by the following
criteria:

* binary package name in alphabetical order (if relevant)
* severity (from highest to lowest)
* tag name (alphabetical order)
* tag details (alphabetical order)

Each tag is represented with the following fields:

* ``tag``: the name of the tag
* ``severity``: one of the possible severities (see below for full list)
* ``package``: the name of the binary or source package (there is no risk
  of confusion between a source and a binary of the same name as the artifact
  with the analysis is dedicated either to a source packages or to a set
  of binary packages, but not to both at the same time)
* ``details``: the details associated to the tag (those are printed after
  the tag name in the lintian output)
* ``explanation``: the long description shown with ``--info``, aka the
  lines prefixed with ``N:``

.. note::

   Here's the ordered list of all the possible severities (from highest
   to lowest):

   * ``error``
   * ``warning``
   * ``info``
   * ``pedantic``
   * ``experimental``
   * ``overridden``
   * ``classification``

   Note that ``experimental`` and ``overridden`` are not true tag
   severities, but lintian's output replaces the usual severity field
   for those tags with ``X`` or ``O`` and it is thus not easily possible
   to capture the original severity.

   And while ``classification`` is implemented like a low-severity issue,
   those tags do not represent real issues, they are just a convenient way
   to export data generated while doing the analysis.

Sbuild task
-----------

Regarding inputs, the ``sbuild`` task is compatible with the ontology
defined for :ref:`package-build-task` even though it implements only
a subset of the possible options at this time.

.. todo::
   Document the outputs of the task (artifact and their relationships)

Internal API: debusine.tasks
============================

.. automodule:: debusine.tasks

.. automodule:: debusine.tasks.sbuild



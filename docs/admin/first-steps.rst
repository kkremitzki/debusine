.. _admin_first_steps:

==========================
First steps using debusine
==========================

Introduction
------------

Set up the pre-requisites using :ref:`Setting up a Debusine server <setting-up-debusine-server>`,
:ref:`Setting up a Debusine worker <setting-up-debusine-worker>` and
:ref:`Setting up a Debusine client <setting-up-debusine-client>`, before
following the next steps.

This section gives an overview of creating and enabling tokens,
submitting a job to Debusine and how to track its status.

Adding and enabling a new worker
--------------------------------

Follow :ref:`Debusine worker instructions <setting-up-debusine-worker>` to add a new worker.

Once the worker has connected to debusine-server, the debusine-admin should
enable the new worker.

Enable a new worker on the server based on the worker's name
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Debusine admin should find the worker, for example:

.. code-block:: console

  $ sudo -u debusine-server debusine-admin list_workers

An output example::

  Name        Registered                        Connected                         Token                                                             Enabled
  ----------  --------------------------------  --------------------------------  ----------------------------------------------------------------  ---------
  saturn      2022-06-13T09:10:21.143146+00:00  2022-06-13T10:26:09.184059+00:00  627ee89ee9b7157295c3acd0461d179ee260eaf4244b82216de3b1a502a15658  True
  mars        2022-06-13T14:25:47.727794+00:00  2022-06-16T07:54:44.288758+00:00  6d5baf211fd96920650495391058bb43e7eb6e12b7510c4fa39c0ac1abf6755e  True
  mercury     2022-06-16T07:54:42.139968+00:00  -                                 93f1b0e9ad2e0ad1fd550f5fdf4ab809594fe1b38ffd37c5b3aa3858062ce0ab  False

In this case, the worker ``mercury`` is the newest addition according to the available
information: it has the latest registered time and is the only one that is not enabled.

The default name of the worker is the FQDN of the worker.

To enable the new worker on the server:

.. code-block:: console

  $ sudo -u debusine-server debusine-admin manage_worker enable mercury

The worker then will be able to connect and process tasks.

Enable a new worker on the server based on the token
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is also possible for the debusine-worker admin to send the token associated
to the worker, to the debusine-server admin. This would allow the
debusine-server admin to enable the worker.

The token can be found in the worker's installation system in the file
``/etc/debusine/worker/token`` or in the logs
(by default, it is in ``/var/log/debusine/worker/worker.log``).

The debusine-server admin could then enable the token using the command:

.. code-block:: console

  $ sudo -u debusine-server debusine-admin manage_token enable 93f1b0e9ad2e0ad1fd550f5fdf4ab809594fe1b38ffd37c5b3aa3858062ce0ab


.. _creating-token-for-debusine-client:

Creating a token for a Debusine client
--------------------------------------

Tokens have an associated user. The first step is to create a debusine user:

.. code-block:: console

  $ sudo -u debusine-server debusine-admin create_user john-smith john@example.com

The password for this user will be printed on the stdout.

The email is required and used to send notifications (e.g. when a job fails)
if the debusine server is configured to send emails, a channel is created
on the server and the work request include the notification settings.

The command ``manage_user`` can be used to change the email. The Django native
command ``changepassword`` can be used to change the password of the user.

Once the user is created, it is possible to create a token for the user. An
optional comment can be added. For example:

.. code-block:: console

  $ sudo -u debusine-server debusine-admin create_token john-smith --comment "Testing Debusine"

The token will be printed to the standard output. The token can be sent to
john-smith and then refer to the section :ref:`Debusine client instructions
<setting-up-debusine-client>` to set the token on the client side.


Listing commands
----------------

The command ``list_tokens`` lists all tokens by default. It is possible
to filter tokens by the owner or the token itself, using the options ``--owner``
or ``--token``, for example:

.. code-block:: console

  $ sudo -u debusine-server debusine-admin list_tokens --owner OWNER
  $ sudo -u debusine-server debusine-admin list_tokens --token TOKEN

The command ``list_workers`` does not have filtering options.

Delete tokens
-------------

Tokens can be removed using the ``delete_tokens`` command. By default, it asks
for interactive confirmation unless ``--yes`` is used. See the options using:

.. code-block:: console

  $ sudo -u debusine-server debusine-admin delete_tokens --help

Other commands
--------------

There are other commands that can be explored using ``--help`` options,
for example:

.. code-block:: console

  $ sudo -u debusine-server edit_worker_metadata --help


Submit a work request
---------------------

Instructions for submitting and checking a work request can be found in the
:ref:`submitting a work request <submitting-a-work-request>` section.

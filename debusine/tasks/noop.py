# Copyright 2022 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""no operation task. Waits and returns. Used in the integration tests."""

from debusine.tasks import Task


class Noop(Task):
    """
    Task that returns a boolean (execute() depending on the result field).

    Used for integration testing.
    """

    TASK_VERSION = 1
    TASK_DATA_SCHEMA = {
        "type": "object",
        "properties": {
            "result": {"type": "boolean"},
        },
        "required": ["result"],
    }

    def __init__(self):
        """Initialize the noop task."""
        super().__init__()

    def execute(self) -> bool:
        """Return self.data['result'] (was sent by the client)."""
        return self.data['result']

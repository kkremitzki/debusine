.. _team-practices:

==============
Team practices
==============

This page documents how we intend to work as a team. It's particulary
relevant for developers paid by Freexian towards the achievement of the
Freexian defined milestones, but can be useful for others too.

Roles
-----

This documentation refers to a few different roles:

* Developer: implements the requested features
* Project manager: coordinates the team and the work
* Architect: takes the biggest design decisions (interfaces,
  software's structure, choices of technologies)
* Community manager: manages relationships with Debian and other
  free software communities

Each team member can have multiple roles.

Project management
------------------

The project managers ensure that the backlog (aka `issues labelled
"next"
<https://salsa.debian.org/freexian-team/debusine/-/issues?label_name%5B%5D=Next>`_)
always contains issues that can be picked up by developers. They select
the issues in a way that follows the milestones laid out by Freexian.

The project managers also schedule design work for the architects when the
next features to develop do require such preliminary work.

They also share regular status updates to the team.

Development workflow
--------------------

The developers can self-assign issues from the "Next" columm in the main
`development board
<https://salsa.debian.org/freexian-team/debusine/-/boards>`_ and move them
to the "In progress" column.

Other guidelines relative to development can be found in the sections
:ref:`contributing` and :ref:`coding-practices`.

You are encouraged to create draft merge requests quite early in your
work, in particular when you have doubts about some design choices and
would like to have feedback.

About code reviews
------------------

Code reviews are mandatory to get any significant code contribution
merged. Developers should thus always submit merge requests for any non-trivial
change.

Developers should priorize code reviews over development work so as to not
stall development work for others, and ensure timely and regular merge of
new code.

The final merge should always be done by a senior developer. But (initial)
code reviews can be picked by any developer. We highly encourage junior
developers to do such code reviews so as to get familiar with more parts
of the codebase. To avoid duplication of work, developers should mark
themselves as reviewer when they start a review.

During code reviews, you should ensure that:

* coding practices have been respected
* unit tests are meaningful and readable without (too much) duplication
* the code actually implements the initial request

You can also suggest improvements and/or simplifications based on your own
experience and knowledge of the codebase.

Coordination and communication tools
------------------------------------

The bulk of the work happens asynchronously and transparently on GitLab
issues and merge requests. But we have also have real time communication
channels with IRC and Matrix.

GitLab
~~~~~~

Watching the whole project is not required. Depending on your preferences,
you could however configure custom notifications to watch at least "New
merge request" and "Merge merge request" events.

You also have the possibility to subscribe to issues with a specific
label by clicking on the "Subscribe" button next to each label
on the `Labels page
<https://salsa.debian.org/freexian-team/debusine/-/labels>`_. Following
"Design discussion" can be interesting if you want to contribute to the
overall design.

IRC channels / Matrix
~~~~~~~~~~~~~~~~~~~~~

Presence on the #debusine IRC channel (or the corresponding Matrix room)
is highly recommended to facilitate real time cooperation and
coordination. For instance, it can be used to share the work load of code
reviews, so that they are spread across all developers. It can also be
used to avoid code conflicts when two developers are working on related
parts.

The #debusine-notifications IRC channel (or the corresponding Matrix room)
offers an alternate way to consume the GitLab-based notifications (pushes,
merge requests, issues).

Current team organization
-------------------------

People in bold are the main actors for each role. The others are in support
or as backup when the main person is not available.

* Developer

  * Carles Pina (senior)
  * Enrico Zini (senior)
  * Jochen Sprickerhof
  * Sylvain Beucler
  * Emilio Pozuelo Monfort
  * Stefano Rivera

* Project manager

  * **Stefano Rivera**
  * Raphaël Hertzog

* Architect

  * **Raphaël Hertzog**
  * Stefano Rivera

* Community manager

  * **Enrico Zini**
  * Stefano Rivera

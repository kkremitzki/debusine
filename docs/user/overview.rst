.. _overview:

=================
Debusine overview
=================

What is Debusine
----------------

Debusine manages scheduling and distribution of Debian-related tasks to
distributed worker machines.

It is being developed by Freexian with the intention of giving people
access to a range of pre-configured tools and workflows running on remote
hardware.

Tasks can be very diverse: from package build and QA checks, to
collecting distribution-wide data like ``.desktop`` files or source code
statistics.

We want to make it as easy as possible for Debian contributors to use all the
QA tools that Debian provides. We want to build the next generation of Debian’s
build infrastructure, one that will continue to reliably do what it already
does, but that will also enable distribution-wide experiments, custom package
repositories and custom workflows with advanced package reviews.


Debusine for Debian development
-------------------------------

One of the first milestones of Debusine development will be to give Debian
developers an easy way to run tools such as sbuild_, autopkgtest_, lintian_,
blhc_ and piuparts_, on the packages they are working on, using existing and
well configured worker machines.

This is a low-hanging fruit to give people access to Debusine very early on in
its development so that it can have input from Debian developers, and provide
resources for Debian development, at all stages.

Since Debusine will be able to build packages and store built artifacts, this
can turn out to be a convenient infrastructure to implement PPA/bikesheds
for Debian.

.. _sbuild: https://wiki.debian.org/sbuild
.. _autopkgtest: https://wiki.debian.org/ContinuousIntegration/autopkgtest
.. _lintian: https://wiki.debian.org/Lintian
.. _blhc: https://ruderich.org/simon/blhc/
.. _piuparts: https://wiki.debian.org/piuparts


Next generation Debian infrastructure
-------------------------------------

One of the milestones we're working on is to evolve Debusine into a possible
replacement for wanna-build and buildd, both for Freexian's internal use and for use by Debian.

Debusine will support creating pipelines with build, QA, approval and signature
steps that are triggered by package uploads. Pipelines will understand the
distinction between official Debian workers and external workers, and only
schedule builds targeting official distribution on official workers.



Run distribution-wide QA experiments
------------------------------------

The distributed design of Debusine is also intended to give any Debian
developer the possibility to run archive-wide QA experiments, like full
rebuilds, using cloud resources.

Debian-friendly people and companies will be able to sponsor external computing
time, while trusted builds will remain under Debian's control.


Current development plan
------------------------

Freexian obtained STF_ funding for a substantial set of milestones, so
development is happening on a clear schedule.

You can see the funded milestones and their progress directly in the
`Debusine project on Salsa <https://salsa.debian.org/freexian-team/debusine/-/milestones>`_

.. _STF: https://sovereigntechfund.de/en/


Learn more
----------

* :ref:`admin_first_steps`
* :ref:`design_faq`
* :ref:`contributing`

..
  TODO: ok, someone arrived here and we have their attention: what would we
  want them to read next?

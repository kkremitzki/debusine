# Copyright 2022 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Utility Functions used by debusine client."""

import requests

from debusine.client import exceptions


def requests_put_or_connection_error(
    *args, **kwargs
) -> requests.models.Response:
    r"""Return requests.put(\*args, \*\*kwargs) or ClientConnectionError."""
    try:
        return requests.put(*args, **kwargs)
    except requests.exceptions.RequestException as exc:
        raise exceptions.ClientConnectionError(
            f"Cannot connect to {exc.request.url}. Error: {str(exc)}"
        )
